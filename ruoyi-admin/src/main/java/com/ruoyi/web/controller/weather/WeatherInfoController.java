package com.ruoyi.web.controller.weather;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.entity.Weather;
import com.ruoyi.system.service.IWeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;
import java.util.Map;

/**
 * 天气信息
 * 
 * @author lilei
 */
@Controller
@RequestMapping("/weather")
public class WeatherInfoController extends BaseController
{
    private String prefix = "system/role";

    @Autowired
    private IWeatherService weatherService;

//    @RequiresPermissions("system:role:view")
//    @GetMapping()
//    public String role()
//    {
//        return prefix + "/role";
//    }

//    @RequiresPermissions("system:role:list")
    @PostMapping("/getLastSevenInfo")
    @ResponseBody
    public List<Weather> getLastSevenInfo()
    {
//        startPage();
        List<Weather> list = weatherService.getLastSevenInfo("340100");
        return list;
    }
//    @RequiresPermissions("system:role:list")
    @PostMapping("/getTodayWeatherInfo")
    @ResponseBody
    public List<Weather> getTodayWeatherInfo()
    {
//        startPage();
        List<Weather> list = weatherService.getTodayWeatherInfo();
        return list;
    }

    @PostMapping("/getTodayTemplate")
    @ResponseBody
    public Map<String, Integer> getTodayTemplate()
    {
//        startPage();
        Map<String, Integer> map = weatherService.getTodayTemplate();
        return map;
    }


}