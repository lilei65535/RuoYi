package com.ruoyi.common.core.domain.entity;

import com.ruoyi.common.core.domain.BaseEntity;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * weather
 * @author 
 */
@Data
public class Weather extends BaseEntity{
    /**
     * ID
     */
    private Integer id;

    /**
     * 省
     */
    private String province;

    /**
     * 市
     */
    private String city;

    /**
     * 市id
     */
    private String cityId;

    /**
     * 温度（℃）
     */
    private Integer temp;

    /**
     * 体感温度(℃)
     */
    private Integer feelsLike;

    /**
     * 相对湿度(%)
     */
    private Integer rh;

    /**
     * 风力等级
     */
    private String windClass;

    /**
     * 风向描述
     */
    private String windDir;

    /**
     * 天气现象
     */
    private String text;

    /**
     * 最高温度(℃)
     */
    private Integer high;

    /**
     * 最低温度(℃)
     */
    private Integer low;

    /**
     * 白天风力
     */
    private String wcDay;

    /**
     * 晚上风力
     */
    private String wcNight;

    /**
     * 白天风向
     */
    private String wdDay;

    /**
     * 晚上风向
     */
    private String wdNight;

    /**
     * 白天天气现象
     */
    private String textDay;

    /**
     * 晚上天气现象
     */
    private String textNight;

    /**
     * 是否删除
     */
    private Integer isDel;

    /**
     * 创建人
     */
    private String createdBy;

    /**
     * 创建时间
     */
    private Date createdTime;

    /**
     * 更新人
     */
    private String updatedBy;

    /**
     * 更新时间
     */
    private Date updatedTime;

    /**
     * 时间
     */
    private String dateStr;

    private static final long serialVersionUID = 1L;
}