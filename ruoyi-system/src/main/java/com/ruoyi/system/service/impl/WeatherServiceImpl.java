package com.ruoyi.system.service.impl;

import com.ruoyi.common.core.domain.entity.Weather;
import com.ruoyi.system.mapper.WeatherMapper;
import com.ruoyi.system.service.IWeatherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lilei
 * @title: WeatherServiceImpl
 * @description: TODO
 * @date 2023/3/6 9:21
 */
@Service
public class WeatherServiceImpl implements IWeatherService {
    @Autowired
    private WeatherMapper weatherMapper;

    @Override
    public List<Weather> getLastSevenInfo(String cityNo) {
        List<Weather> list = weatherMapper.getLastSevenInfo(cityNo);
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd");
        for (Weather weather : list) {
            weather.setDateStr(sdf.format(weather.getCreatedTime()));
        }
        return list;
    }

    @Override
    public List<Weather> getTodayWeatherInfo() {
        List<Weather> list = weatherMapper.getTodayWeatherInfo();
        for (Weather weather : list) {
            if(weather.getProvince().endsWith("省")){
                weather.setProvince(weather.getProvince().split("省")[0]);
            }else if(weather.getProvince().endsWith("市")){
                weather.setProvince(weather.getProvince().split("市")[0]);
            }else if(weather.getProvince().equals("内蒙古自治区")){
                weather.setProvince("内蒙古");
            }else if(weather.getProvince().endsWith("自治区")){
                weather.setProvince(weather.getProvince().substring(0,2));
            }
        }
        return list;
    }

    @Override
    public Map<String, Integer> getTodayTemplate() {
        Map<String, Integer> map = new HashMap<>();
        map.put("max",weatherMapper.getTodayTemplateMax());
        map.put("min",weatherMapper.getTodayTemplateMin());
        return map;
    }
}
