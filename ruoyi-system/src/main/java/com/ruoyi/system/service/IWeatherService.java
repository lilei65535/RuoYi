package com.ruoyi.system.service;

import com.ruoyi.common.core.domain.entity.Weather;

import java.util.List;
import java.util.Map;

/**
 * @author lilei
 * @title: IWeatherService
 * @description: TODO
 * @date 2023/3/6 9:21
 */
public interface IWeatherService {
    List<Weather> getLastSevenInfo(String cityNo);

    List<Weather> getTodayWeatherInfo();

    Map<String, Integer> getTodayTemplate();

}
