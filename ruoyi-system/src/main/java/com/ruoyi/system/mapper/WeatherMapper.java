package com.ruoyi.system.mapper;

import com.ruoyi.common.core.domain.entity.Weather;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @author lilei
 * @title: WeatherMapper
 * @description: TODO
 * @date 2023/3/6 9:23
 */
public interface WeatherMapper {

  int deleteByPrimaryKey(Integer id);

  int insert(Weather record);

  int insertSelective(Weather record);

  Weather selectByPrimaryKey(Integer id);

  int updateByPrimaryKeySelective(Weather record);

  int updateByPrimaryKey(Weather record);

//  @Select("select name cityName, alias cityNo from sms_domain_city " +
//          "where (parent != 0 and parent != 1) and id < 6590 order by id asc")
//  List<CityInfo> getCityList();

  @Select("select * from weather where city = #{cityName} order by CREATED_TIME desc limit 1")
  Weather getTodayWeather(String cityName);

//  @Select("select * from " +
//          "(select * from weather " +
//          "where city_id = #{cityNo} order by created_time desc limit 7" +
//          ") tmp " +
//          "ORDER BY created_time asc")
  List<Weather> getLastSevenInfo(String cityNo);

  List<Weather> getTodayWeatherInfo();

    Integer getTodayTemplateMax();

    Integer getTodayTemplateMin();
}
